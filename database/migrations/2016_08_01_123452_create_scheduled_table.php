<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScheduledTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scheduled', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('lead_id')->nullable();
			$table->string('location_name', 30)->nullable();
			$table->string('city', 30)->nullable();
			$table->string('address_1', 100)->nullable();
			$table->string('address_2', 100)->nullable();
			$table->string('schedule_date', 10)->nullable();
			$table->string('schedule_time', 10)->nullable();
			$table->string('schedule_note', 500)->nullable();
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('initiated')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scheduled');
	}

}
