<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name', 100)->nullable();
			$table->string('last_name', 100)->nullable();
			$table->string('email', 200)->nullable();
			$table->integer('phone')->nullable();
			$table->integer('zip')->nullable();
			$table->integer('dob')->nullable();
			$table->integer('height')->nullable();
			$table->string('gender', 6)->nullable();
			$table->string('tobacco', 5)->nullable();
			$table->string('hypertension', 5)->nullable();
			$table->string('hypertensionmedication', 5)->nullable();
			$table->string('groupone', 5)->nullable();
			$table->string('diabetes', 5)->nullable();
			$table->string('diseases', 5)->nullable();
			$table->string('aidsHiv', 5)->nullable();
			$table->string('addiction', 5)->nullable();
			$table->string('criminalCharges', 5)->nullable();
			$table->string('disabilityBenefits', 5)->nullable();
			$table->string('consent', 5)->nullable();
			$table->string('contactDay', 10)->nullable();
			$table->string('contactTime', 9)->nullable();
			$table->string('terms', 9)->nullable();
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('viewed')->nullable()->default(0);
			$table->boolean('scheduled')->nullable()->default(0);
			$table->string('note', 500)->nullable();
			$table->dateTime('viewed_date')->nullable();
			$table->boolean('converted')->nullable()->default(2);
			$table->string('price', 10)->nullable();
			$table->dateTime('date_converted')->nullable();
			$table->boolean('called')->nullable()->default(0);
			$table->boolean('cancelled')->nullable()->default(0);
			$table->string('transaction_id', 30)->nullable();
			$table->boolean('defered')->nullable()->default(0);
			$table->boolean('payed')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicants');
	}

}
