<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentBatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_batch', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('batch_id', 13)->nullable();
			$table->integer('lead_id')->nullable();
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('payed')->nullable()->default(0);
			$table->string('total', 10)->nullable()->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_batch');
	}

}
