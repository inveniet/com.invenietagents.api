$(function(){
  $( ".datepicker" ).datepicker();
  $('.timePicker').timepicker({
    'minTime': '8:00am',
    'maxTime': '11:30pm'
    });

    $("input,select,textarea").jqBootstrapValidation();
});
