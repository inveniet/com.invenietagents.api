<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <script src="https://use.fontawesome.com/76eb79f76b.js"></script>
        <script src="/js/jquery-2.2.4.min.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/bootstrap.js"></script>
        <script src="/js/jquery.timepicker.min.js"></script>
        <script src="/js/jqBootstrapValidation.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/jquery.timepicker.css" rel="stylesheet" type="text/css">
        <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>


      @include('components.navbar')

      <div class="container-fluid ">
        @yield('content')
      </div>
      <script src="/js/main.js"></script>
    </body>
</html>
