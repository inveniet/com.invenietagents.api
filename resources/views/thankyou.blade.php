@extends('layouts.dashboard')

@section('title', 'Thank You')

@section('content')
<div class="row">
  <div class="col-md-offset-4 col-md-4 col-mdoffset-4">
    <div class="panel panel-success">

      <div class="panel-heading">
        <h3>Thank you!</h3>
      </div>

      <div class="panel-body">
        Your payment of {{$amount}} has been recieved for batch {{$batch_id}}.
      </div>

    </div>
  </div>
</div>
@endsection
