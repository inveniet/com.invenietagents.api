<form type="GET" action="{{route('payLead')}}">
<table class="table">
  <thead>
    <tr>
      <th><div class="btn btn-default"><i class="fa fa-check-square-o" aria-hidden="true"></i></div></th>
      <th>ID</th>
      <th>Client Name</th>
      <th>Date Converted</th>
      <th>Transaction_id</th>
      <th style="text-align: right">Balance</th>
    </tr>
  </thead>
  <tbody>
  @foreach($unpaid as $unpaidlead)
    <tr>
      <td><input type="checkbox" name="paying[]" value="{{$unpaidlead->id}}" /></td>
      <td><a href="{{route('convertedInfo', [$unpaidlead->id])}}">{{$unpaidlead->id}}</a></td>
      <td>{{$unpaidlead->first_name . " " . $unpaidlead->last_name}}</td>
      <td>{{$unpaidlead->date_converted}}</td>
      <td>{{$unpaidlead->transaction_id}}</td>
      <td style="text-align: right">${{$unpaidlead->price}}</td>
    </tr>
  @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="2"><button type="submit" class="btn btn-success btn-block btn-lg">Pay Now <i class="fa fa-paypal" aria-hidden="true"></i> </button></td>
      <td colspan="2"></td>
      <td style="text-align: right">
        <strong>Total Due:</strong>
      </td>
      <td style="text-align: right">
        ${{$unpaidTotal}}
      </td>
    </tr>

  </tfoot>
</table>
</form>
