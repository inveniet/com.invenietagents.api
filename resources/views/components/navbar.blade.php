<!-- Navbar Start -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand {{Route::currentRouteName() == 'overview' ? 'active': ''}}" href="{{route('overview')}}">Dashboard</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{Route::currentRouteName() == 'home' ? 'active': ''}}">
          <a href="{{route('home')}}">Leads</a>
        </li>
        <li class="{{Route::currentRouteName() == 'scheduled' ? 'active': ''}}">
          <a href="{{route('scheduled')}}">Scheduled</a>
        </li>
        <li class="{{Route::currentRouteName() == 'converted' ? 'active': ''}}">
          <a href="{{route('converted')}}">Converted</a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#">Declined/Cancelled</a></li> -->
        <li><a href="{{ url('/logout') }}">Logout</a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

</nav>
<!-- Navbar End-->
