<table class="table">
  <thead>
    <tr>
      <th>Batch ID</th>
      <th>Date Payed</th>
      <th style="text-align: right;">Price</th>
    </tr>
  </thead>
  <tbody>
  @foreach($paidBatches as $paidBatch)
    <tr>
      <td><a href="{{route('convertedInfo', [$paidBatch->batch_id, true])}}">{{$paidBatch->batch_id}}</a></td>
      <td>{{$paidBatch->created}}</td>
      <td style="text-align:right;">{{$paidBatch->total}}</td>
    </tr>
  @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="2" style="text-align: right">
        <strong>Total Payed Out TD <i class="fa fa-sign-language" aria-hidden="true"></i>:</strong>
      </td>
      <td style="text-align: right;">
        ${{$paidTotal}}
      </td>
    </tr>
  </tfoot>
</table>
