
<div class="panel panel-default">
  <table class="table">
    <tr>
    <th>Tobaco User</th>
    <td>{{$lead->tobacco}}</td>
    </tr>
  </table>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h5>In the last 5 years has been treated for:</h5>
  </div>
  <table class="table">
    <tr>
      <th>Hypertension, High Blood Pressure</th>
      <td>{{$lead->hypertension}}</td>

    </tr>
    <tr>
      <th>-Taking 3 or more medications for: Hypertension, High Blood Pressure</th>
      <td>{{$lead->hypertensionmedication}}</td>
    </tr>
    <tr>
      <th>Stroke,
Cancer,
Tumors,
Paralysis,
Multiple Sclerosis,
Lupus,
Rheumatoid Arthritis,
Muscular Dystrophy,
Leukemia,
Lymphoma,
Seizures,
Mental or Nervous Disorder,
Scleroderma</th>
      <td>
        {{$lead->groupone}}
      </td>
    </tr>
    <tr>
      <th>Diabetes</th>
      <td>{{$lead->diabetes}}</td>
    </tr>
  </table>
</div>

<div class="panel panel-default">
  <table class="table">
    <tr>
    <th>Treated or diagnosed for:
      Heart Disease
Liver - Including Hepatitis C
Pancreas
Blood
Brain
Kidney
Neurological or Nervous System
Circulatory
Respiratory - Including Sleep Apnea
Gastrointestinal
    </th>
    <td>{{$lead->diseases}}</td>
    </tr>
  </table>
</div>

<div class="panel panel-default">
  <table class="table">
    <tr>
    <th>has HIV or AIDS</th>
    <td>{{$lead->aidsHiv}}</td>
    </tr>
    <tr>
    <th>Received professional counseling or medical treatment due to the use of alcohol or drugs or have been convicted for any illegally used drugs or alcohol related charges</th>
    <td>{{$lead->addiction}}</td>
    </tr>
  </table>
</div>

<div class="panel panel-default">
  <table class="table">
    <tr>
    <th>Pled guilty or been convicted of a felony in the last 5 years or have any pending criminal charges</th>
    <td>{{$lead->criminalCharges}}</td>
    </tr>
    <tr>
    <th>In last 3 years has received disability benefits for a period of 6 months or longer, or is currently receiving disability benefits</th>
    <td>{{$lead->disabilityBenefits}}</td>
    </tr>
  </table>
</div>
