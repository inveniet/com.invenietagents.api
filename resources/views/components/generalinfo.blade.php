<table class="table">
<tr>
  <th>Location</th>
  <th>Gender</th>
  <th>Height</th>
  <th>Date Of Birth</th>
  <th>Contact Time</th>
</tr>
<tr>
  <td><a href="https://www.google.com/maps/place/{{$lead->zip}}" target="_blank">{{$lead->zip}}</td>
  <td>{{$lead->gender}}</td>
  <td>{{$lead->height}}</td>
  <td>{{$lead->dob}}</td>
  <td><strong>{{$lead->contactDay}}</strong> between <strong>{{$lead->contactTime}}</strong></td>
</tr>
</table>
