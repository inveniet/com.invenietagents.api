@extends('layouts.dashboard')

@section('title', 'Converted')

@section('content')

<!-- Wrapper  -->
<div class="container-fluid">
  <div class="row">
    <!-- Spread Sheets Start -->
    <div class="col-md-6">

      <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="{{($showing_batch ? '' : 'active')}}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Unpaid</a></li>
    <li role="presentation" class="{{($showing_batch ? 'active' : '')}}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Paid</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane {{($showing_batch ? '' : 'active')}}" id="home">
      <br />
      <div class="panel panel-default">
        @include('components.unpaidleads')
      </div>
    </div>
    <div role="tabpanel" class="tab-pane {{($showing_batch ? 'active' : '')}}" id="profile">
      <br />
      <div class="panel panel-default">
        @include('components.paidleads')
      </div>
    </div>
  </div>

</div>



    </div>
    <!-- Spread Sheets End -->


    <!-- Detailed Item Start -->
    <div class="col-md-6">


      @if(count($paidLeads) AND $paidLeads !== false)
      <!-- colapse start -->
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      @foreach($paidLeads as $key=>$paidLead)
      <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$key}}" aria-expanded="{{($key < 1)}}" aria-controls="collapseOne">
          {{$paidLead['lead']->created}} <span style="float:right">${{$paidLead['lead']->price}}</span>
        </a>
      </h4>
    </div>
    <div id="collapse_{{$key}}" class="panel-collapse collapse {{($key < 1 ? 'in' : '')}}" role="tabpanel" aria-labelledby="heading_{{$key}}">
      <div class="panel-body">

      <div class="row">
        <div class="col-md-6">
          <h1>{{$paidLead['lead']->first_name}} {{$paidLead['lead']->last_name}}</h1>

          <h4>{{$paidLead['appointment']->city}}</h4>
          <p>
             {{$paidLead['appointment']->address_1}}<br />
             {{$paidLead['appointment']->address_2}}<br />
             {{$paidLead['lead']->zip}}
          </p>
          <p>
            <strong>Email:</strong> {{$paidLead['lead']->email}}<br />
            <strong>Phone:</strong> {{$paidLead['lead']->phone}}
          </p>
          <div class="well">
            {{$paidLead['lead']->note}}
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h1>Price: <span style="float: right">${{$paidLead['lead']->price}}</span></h1>
            </div>
            <ul class="list-group">
              <li class="list-group-item">Create: <span style="float:right;">{{$paidLead['lead']->created}}</span></li>
              <li class="list-group-item">Converted: <span style="float:right;">{{$paidLead['lead']->date_converted}}</span></li>
              <li class="list-group-item">Paid: <span style="float:right;" class="label {{$paidLead['lead']->payed == 1 ?'label-success':'label-danger'}}">{{$paidLead['lead']->payed == 1 ?'yes':'No'}}</span></li>
              <li class="list-group-item">ID: <span style="float:right;">{{$paidLead['lead']->id}}</span></li>
              <li class="list-group-item">Transaction ID: <span style="float:right;">{{$paidLead['lead']->transaction_id}}</span></li>
            </ul>
          </div>
        </div>
    </div>


    <!-- Tabbed Start -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general_{{$key}}" aria-controls="general_{{$key}}" role="tab" data-toggle="tab">General Info</a></li>
      <li role="presentation"><a href="#medical_{{$key}}" aria-controls="medical_$key}}" role="tab" data-toggle="tab">Medical Info</a></li>
    </ul>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general_{{$key}}">
        <br />
        @include('components.generalinfo', ["lead" => $paidLead['lead']])
      </div>
      <div role="tabpanel" class="tab-pane" id="medical_{{$key}}">
        <br />
        @include('components.medicalinfo', ["lead" => $paidLead['lead']])
      </div>
    </div>
    <!-- Tabbed End -->
  </div>
</div>
</div>
    @endforeach
    </div>
    @else
    <h3>Select item by clicking on the id for more details.</h3>
    @endif
    </div>
    <!-- Detailed Item End -->

  </div>
</div>
<!-- Wrapper End -->
@endsection
