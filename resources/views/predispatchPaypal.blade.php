@extends('layouts.empty')
@section('title', 'Redirecting...')

@section('content')
<!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> -->
<div class='row'>
  <div class="col-md-offset-4 col-md-4 col-md-offset-4">
    <br />
    <br />
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h1>We're sending you to PayPal</h1>
        <h2>One second please...</h2>
      </div>
      <div class="panel-body" style="min-height: 300px">
        <div id="loader">
            <div id="box"></div>
            <div id="hill"></div>
        </div>

      </div>
      <div class="panel-footer">
        <form action="{{$paypal_link}}" method="get" target="_top" id="paypal">
        <input type="hidden" name="cmd" value="{{$paypal_cmd}}">
        <input type="hidden" name="hosted_button_id" value="{{$paypal_hosted_button_id}}">
        <input type="hidden" name="amount" value="{{$total}}">
        <input type="hidden" name="invoice" value="{{$batch_id}}">
        <input type="hidden" name="at" value="{{$paypal_token}}">
        <input type="hidden" name="item_name" value="Batched Pay: {{$batch_id}}">
        <input type="hidden" name="item_number" value="{{$batch_id}}">
        <input type="submit" class="btn btn-default btn-block"  value="If you're not redirected in 5 seconds, click here." >
        </form>



      </div>
    </div>
  </div>
</div>


<style>

#loader {
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -2.7em;
  margin-left: -2.7em;
  width: 5.4em;
  height: 5.4em;
}

#hill {
  position: absolute;
  width: 7.1em;
  height: 7.1em;
  top: 1.7em;
  left: 1.7em;
  background-color: transparent;
  border-left: .25em solid rgb( 55, 123, 181);
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#hill:after {
  content: '';
  position: absolute;
  width: 7.1em;
  height: 7.1em;
  left: 0;

}

#box {
  position: absolute;
  left: 0;
  bottom: -.1em;
  width: 1em;
  height: 1em;
  background-color: transparent;
  border: .25em solid rgb( 55, 123, 181);
  border-radius: 15%;
  -webkit-transform: translate(0, -1em) rotate(-45deg);
  -moz-transform: translate(0, -1em) rotate(-45deg);
  -ms-transform: translate(0, -1em) rotate(-45deg);
  transform: translate(0, -1em) rotate(-45deg);
  animation: push 2.5s cubic-bezier(.79, 0, .47, .97) infinite;
}

@-webkit-keyframes push {
  0% {
      -webkit-transform: translate(0, -1em) rotate(-45deg);
  }
  5% {
      -webkit-transform: translate(0, -1em) rotate(-50deg);
  }
  20% {
      -webkit-transform: translate(1em, -2em) rotate(47deg);
  }
  25% {
      -webkit-transform: translate(1em, -2em) rotate(45deg);
  }
  30% {
      -webkit-transform: translate(1em, -2em) rotate(40deg);
  }
  45% {
      -webkit-transform: translate(2em, -3em) rotate(137deg);
  }
  50% {
      -webkit-transform: translate(2em, -3em) rotate(135deg);
  }
  55% {
      -webkit-transform: translate(2em, -3em) rotate(130deg);
  }
  70% {
      -webkit-transform: translate(3em, -4em) rotate(217deg);
  }
  75% {
      -webkit-transform: translate(3em, -4em) rotate(220deg);
  }
  100% {
      -webkit-transform: translate(0, -1em) rotate(-225deg);
  }
}

@-moz-keyframes push {
  0% {
      -moz-transform: translate(0, -1em) rotate(-45deg);
  }
  5% {
      -moz-transform: translate(0, -1em) rotate(-50deg);
  }
  20% {
      -moz-transform: translate(1em, -2em) rotate(47deg);
  }
  25% {
      -moz-transform: translate(1em, -2em) rotate(45deg);
  }
  30% {
      -moz-transform: translate(1em, -2em) rotate(40deg);
  }
  45% {
      -moz-transform: translate(2em, -3em) rotate(137deg);
  }
  50% {
      -moz-transform: translate(2em, -3em) rotate(135deg);
  }
  55% {
      -moz-transform: translate(2em, -3em) rotate(130deg);
  }
  70% {
      -moz-transform: translate(3em, -4em) rotate(217deg);
  }
  75% {
      -moz-transform: translate(3em, -4em) rotate(220deg);
  }
  100% {
      -moz-transform: translate(0, -1em) rotate(-225deg);
  }
}

@-ms-keyframes push {
  0% {
      -ms-transform: translate(0, -1em) rotate(-45deg);
  }
  5% {
      -ms-transform: translate(0, -1em) rotate(-50deg);
  }
  20% {
      -ms-transform: translate(1em, -2em) rotate(47deg);
  }
  25% {
      -ms-transform: translate(1em, -2em) rotate(45deg);
  }
  30% {
      -ms-transform: translate(1em, -2em) rotate(40deg);
  }
  45% {
      -ms-transform: translate(2em, -3em) rotate(137deg);
  }
  50% {
      -ms-transform: translate(2em, -3em) rotate(135deg);
  }
  55% {
      -ms-transform: translate(2em, -3em) rotate(130deg);
  }
  70% {
      -ms-transform: translate(3em, -4em) rotate(217deg);
  }
  75% {
      -ms-transform: translate(3em, -4em) rotate(220deg);
  }
  100% {
      -ms-transform: translate(0, -1em) rotate(-225deg);
  }
}

@keyframes push {
  0% {
      transform: translate(0, -1em) rotate(-45deg);
  }
  5% {
      transform: translate(0, -1em) rotate(-50deg);
  }
  20% {
      transform: translate(1em, -2em) rotate(47deg);
  }
  25% {
      transform: translate(1em, -2em) rotate(45deg);
  }
  30% {
      transform: translate(1em, -2em) rotate(40deg);
  }
  45% {
      transform: translate(2em, -3em) rotate(137deg);
  }
  50% {
      transform: translate(2em, -3em) rotate(135deg);
  }
  55% {
      transform: translate(2em, -3em) rotate(130deg);
  }
  70% {
      transform: translate(3em, -4em) rotate(217deg);
  }
  75% {
      transform: translate(3em, -4em) rotate(220deg);
  }
  100% {
      transform: translate(0, -1em) rotate(-225deg);
  }
}
</style>

<script>
$(function(){
  setTimeout(function(){
    $("#paypal").submit();
  }, 1000);
});
</script>

@endsection
