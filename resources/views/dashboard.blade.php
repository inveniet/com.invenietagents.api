@extends('layouts.dashboard')

@section('title', 'Leads')

@section('content')


  @if(count($applicants))
  <div class="row">
    <div class="col-md-2">

      @include('sidebars.leads')

    <div class="media">
      <div class="media-body">
        <h5><span class="label label-success"><i class="fa fa-phone" aria-hidden="true"></i></span> Unresolved Call</h5>
      </div>
    </div>
  </div>



  <div class="col-md-10">
    @if(!$lead)
    <h3>Select an Applicant</h3>
    @else
    <div class="row">
      <div class="col-md-4">
        <h1>{{$lead->first_name}} {{$lead->last_name}}</h1>

      </div>
      <div class="col-md-8">
        <table class="table">
        <tr>
          <th>Location</th>
          <th>Gender</th>
          <th>Height</th>
          <th>Date Of Birth</th>
          <th>Contact Time</th>
        </tr>
        <tr>
          <td><a href="https://www.google.com/maps/place/{{$lead->zip}}" target="_blank">{{$lead->zip}}</td>
          <td>{{$lead->gender}}</td>
          <td>{{$lead->height}}</td>
          <td>{{$lead->dob}}</td>
          <td><strong>{{$lead->contactDay}}</strong> between <strong>{{$lead->contactTime}}</strong></td>
        </tr>
      </table>

    </div>
    @if($lead->called == 1)
    <div class="container">
    <table class="table">
      <tr>
        <th>Email</th>
        <th>Phone</th>
      </tr>
      <tr>
        <td>{{$lead->email}}</td>
        <td>{{$lead->phone}}</td>
      </tr>
    </table>
  </div>
    @endif
    </div>

    @include('components.medicalinfo')

  <div class="row">
    <div class="col-md-3">
      @if($lead->called == 1)
      <a class="btn btn-warning btn-block btn-lg" data-container="body" data-toggle="modal" data-target="#cancelForm">Cancel Lead</a>
      @endif
    </div>
    <div class="col-md-3">
      @if($lead->called == 1)
      <a class="btn btn-default btn-block btn-lg" data-toggle="modal" data-target="#scheduleForm">Schedule</a>
      @endif
    </div>
    <div class="col-md-3">

    </div>
    <div class="col-md-3">
      @if($lead->called == 0)
      <a href="{{route('contactLead', [$lead->id])}}" class="btn btn-primary btn-block btn-lg">Contact</a>
      @elseif($lead->scheduled == 0)
        <div class="alert alert-warning">
          This is an unresolved lead.
        </div>
      @endif
    </div>
    </div>

    <!-- include schedule appointment form -->
      @include('forms.scheduleform')
      <!-- include cancel lead form -->
      @include('forms.cancelform')



    @endif
  </div>
</div>
@else
  @include('errors.noleads')
@endif
@endsection
