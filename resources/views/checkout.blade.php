@extends('layouts.dashboard')

@section('title', 'Overview')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h1>Payout Summary</h1>
        </div>
        <div class="panel-body">

        </div>
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Policy Number</th>
              <th style="text-align: right;">Price</th>
            </tr>
          </thead>
          <tbody>
          @foreach($payingList as $payable)
          <tr>
            <td>{{$payable->id}}</td>
            <td>{{$payable->transaction_id}}</td>
            <td style="text-align: right;">${{$payable->price}}</td>
          </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <td colspan="2">Total</td>
              <td style="text-align: right;">${{$total}}</td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h1>Total Ammount Due: <span style="float:right;">${{$total}}</span></h1>
        </div>
        <div class="panel-body">
          <p>
            Clicking below will redirect you to PayPal where you'll finish the transaction.
            Once you've completed the transaction, you will be redirected back to
            this website where you'll receieve a summary of the payment. Please make sure
            you are redirected back to ensure the system marks your payment as payed.
          </p>
          <p>
            <span class="label label-warning">NOTE:</span> If an error occures, please don't try and pay the leads again, please contact
            the system admin and he'll help you out.
          </p>
        </div>
        <div class="panel-footer">
          <form type="GET" action="{{route('prepareBatch')}}">
            @foreach($payingList as $payable)
              <input type="hidden" name="payablelead[]" value="{{$payable->id}}" />
            @endforeach
              <input type="hidden" name="total" value="{{$total}}" />
            <button class="btn btn-success btn-lg btn-block">Pay Now With PayPal <i class="fa fa-paypal" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
