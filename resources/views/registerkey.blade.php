@extends('layouts.dashboard')

@section('title', 'Overview')

@section('content')

<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3><i class="fa fa-key" aria-hidden="true"></i> Please Register With Key Provided</h3>
      </div>
      <div class="panel-body">
        @if(count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach($errors->all() as $error)
                <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <form class="form-horizontal" action="{{route('registerKeyNow')}}" method="GET">
            <div class="form-group">
              <label for="key" class="col-sm-2 control-label">Key</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="key" name="key" placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Register</button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>

@endsection
