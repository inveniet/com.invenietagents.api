@extends('layouts.dashboard')

@section('title', 'Scheduled')

@section('content')



@if(count($schedules))
<div class="row">
  <div class="col-md-2">

@include('sidebars.appointments')

  </div>


  <div class="col-md-10">
    @if(!$appointment)
    <h3>Select an appointment</h3>
    @else
    @if (count($errors)>0)
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-6">
        <h1>{{$lead->first_name}} {{$lead->last_name}}</h1>
        @if($lead->defered == 1)
        <div class="alert alert-info"><strong>Note:</strong> This lead has been defered.</div>
        @else
        <h3>Appointment is {{$appointment->days_left}} at {{$appointment->schedule_time}} in {{$appointment->city}}</h3>
        @endif
        @include('components.contactinfo')
      </div>
      <div class="col-md-6">
        <div class="">
          <ul class="list-group">
            <li class="list-group-item">
              <h4 class="list-group-item-heading"> Meeting at {{$appointment->location_name}} on {{$appointment->schedule_date}}</h4>
              <p class="list-group-item-text"></p>
            </li>
            <li class="list-group-item">
              <div class="row">
                <div class="col-sm-4">
                  <h4 class="list-group-item-heading">Address</h4>
                  <p class="list-group-item-text">{{$appointment->city}}</p>
                  <p class="list-group-item-text">{{$appointment->address_1}}</p>
                  <p class="list-group-item-text">{{$appointment->address_2}}</p>
                  <a href="https://www.google.com/maps/place/{{urlencode($appointment->address_1 . " " . $appointment->address_2  . $appointment->city . 'CA')}}" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> Open in Google Maps</a>
                </div>
                <div class="col-sm-8">

                  <div class="well">
                      {{$appointment->schedule_note}}
                  </div>
                </div>
              </div>
            </li>
          </ul>


        </div>
      </div>

    </div>

    <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#code" aria-controls="code" role="tab" data-toggle="tab">Actions</a></li>
    <li role="presentation"><a href="#general_info" aria-controls="general_info" role="tab" data-toggle="tab">General Info</a></li>
    <li role="presentation"><a href="#medical_info" aria-controls="medical_info" role="tab" data-toggle="tab">Medical Info</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="code">
      <br />

      <div class="row">

        <div class="col-md-4">
          @if($lead->defered != 1)
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3>Defer Conversion</h3>
            </div>
            <div class="panel-body">
              Lead has been submitted for conversion. May take a few days to confirm.
              You may defer this lead and decline or convert it at a later day.
            </div>
            <div class="panel-footer">
              <input type="button" class="btn btn-primary btn-lg btn-block" value="Defer Conversion" data-toggle="modal" data-target="#deferForm">
            </div>
          </div>
          @include('forms.deferForm')
          @endif

          <div class="panel panel-warning">
            <div class="panel-heading">
              <h3>Cancel Lead</h3>
            </div>
            <div class="panel-body">
              Lead has been canceled.
            </div>
            <div class="panel-footer">
              <input type="button" class="btn btn-warning btn-lg btn-block" value="Cancel lead" data-toggle="modal" data-target="#cancelForm">
            </div>
          </div>
          @include('forms.cancelform')
        </div>
        <div class="col-md-4">
          <div class="panel panel-danger">
            <div class="panel-heading">
              <h3>Lead Was Declined</h3>
            </div>
            <div class="panel-body">
              The lead has been declined.
            </div>
            <div class="panel-footer">
              <input type="button" class="btn btn-danger btn-lg btn-block" value="Lead Declined" data-toggle="modal" data-target="#declineForm">
            </div>
          </div>

          @include('forms.declineform')
        </div>
        <div class="col-md-4">
          <div class="panel panel-success">
            <div class="panel-heading">
              <h3>Lead Converted</h3>
            </div>
            <div class="panel-body">
              Lead succesfully converted.
            </div>
            <div class="panel-footer">
              <input type="button" class="btn btn-success btn-lg btn-block" value="Confirm Conversion"  data-toggle="modal" data-target="#convertForm">
            </div>
          </div>
          @include('forms.convertform')
        </div>
      </div>

    </div>
    <div role="tabpanel" class="tab-pane" id="general_info">
      <h2>General Info</h2>
      @include('components.generalinfo')

    </div>
    <div role="tabpanel" class="tab-pane" id="medical_info">
      <h2>Medical Info</h2>
      @include('components.medicalinfo')
    </div>
  </div>

</div>

    @endif
  </div>
</div>
@else

  @include('errors.noscheduled')

@endif
@endsection
