<div class="list-group">

@foreach ($applicants as $applicant)

  @if($lead != false)
    @if($lead->id == $applicant->id)
      <a href="{{route('lead', [$applicant->id])}}" class="list-group-item active">
    @else
      <a href="{{route('lead', [$applicant->id])}}" class="list-group-item">
    @endif
@else
  <a href="{{route('lead', [$applicant->id])}}" class="list-group-item">
@endif
    {{$applicant->first_name}} {{$applicant->last_name}}
    @if($applicant->viewed == 0)
      <span class="badge">New</span>
    @elseif($applicant->called == 1)
      <span class="label label-success" style="float: right;"><i class="fa fa-phone" aria-hidden="true"></i></span>
    @endif
</a>
@endforeach
</div>
