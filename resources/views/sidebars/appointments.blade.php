<!-- Side Bar Start -->
    <div class="list-group">
      <!-- Start Looping through each schedule -->
      @foreach ($schedules as $schedule)
      <!-- Check if this is is the active -->
      @if($appointment != false)
      <!--  If so, mark as active active -->
        @if($appointment->id == $schedule->id)
          <a href="{{route('scheduledAppointemtn', [$schedule->id])}}" class="list-group-item active">
        @else
        <!--  Else leave alone -->
          <a href="{{route('scheduledAppointemtn', [$schedule->id])}}" class="list-group-item">
        @endif
      @else
      <!-- No appointment is selected, just show normal -->
        <a href="{{route('scheduledAppointemtn', [$schedule->id])}}" class="list-group-item">
      @endif
        <!-- Display Name -->
        {{$schedule->first_name}} {{$schedule->last_name}}
        <!-- Display days left -->
        @if($schedule->defered == 1)
            <span class="label label-info" style="float: right;"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
        @else
          <span class="badge">{{$schedule->days_left}}</span>
        @endif
      </a>
      @endforeach
    </div>

    <!-- End Sidebar -->
