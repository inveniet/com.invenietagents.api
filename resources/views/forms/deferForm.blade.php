<!-- Cancel Lead Modal Start -->
<div class="modal fade" id="deferForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Schedule Meeting With {{$lead->first_name}}</h4>
      </div>
      <form class="form-horizontal" type="GET" action="{{route('deferLead')}}" novalidate>
        <div class="modal-body">

          <div class="alert alert-info">
            <strong>Heads Up:</strong> You're about to defer this lead.
          </div>

          <h4>Tell us why you have to defer this lead.</h4>
          <div class="control-group">
            <div class="control">
              <textarea name="message" class="form-control" minlength="50" maxlength="500" rows="3" required></textarea>
              <p class="help-block"></p>
            </div>
          </div>
          <input type="hidden" value="{{$lead->id}}" name="id" />

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Go Back</button>
          <button type="submit" class="btn btn-primary" >Defer Conversion</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- Cancel Lead Modal End -->
