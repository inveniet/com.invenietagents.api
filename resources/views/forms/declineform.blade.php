<!-- Cancel Lead Modal Start -->
<div class="modal fade" id="declineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{$lead->first_name}} has been declined</h4>
      </div>
<form class="form-horizontal" type="GET" action="{{route('declineLead')}}" novalidate>
      <div class="modal-body">


          <div class="alert alert-danger">
            <strong>Warning:</strong> You're about to mark this lead as declined. This cannot be undone.
          </div>


          <h4>Tell us a little bit about why this lead was declined.</h4>
          <div class="control-group">
            <div class="control">
              <textarea name="message" class="form-control" minlength="50" maxlength="500" rows="3" required></textarea>
              <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group control-group">
            <label for="audit_id" class="col-sm-4 control-label">Policy/App#</label>
            <div class="col-sm-8 controls">
              <input type="tel" pattern="\d+" maxLength="10" minLength="10" name="audit_id" class="form-control" id="audit_id" placeholder="" required>
              <p class="help-block"></p>
            </div>
          </div>

          <input type="hidden" value="{{$lead->id}}" name="id" />
          <input type="hidden" value="@if(isset($appointment->id)){{$appointment->id}} @endif" name="app_id" />

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Go Back</button>
          <button type="submit" class="btn btn-danger" >Decline Lead</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- Cancel Lead Modal End -->
