<!-- Cancel Lead Modal Start -->
<div class="modal fade" id="convertForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{$lead->first_name}} has been Approved! <i class="fa fa-beer" aria-hidden="true"></i></h4>
      </div>
<form class="form-horizontal" type="GET" action="{{route('convertLead')}}" novalidate>
      <div class="modal-body">


          <div class="alert alert-success">
            <strong>Note:</strong> You're about to mark this lead as converted. This cannot be undone.
          </div>


          <h4>Tell us a little bit about why this lead was approved.</h4>
          <div class="control-group">
            <div class="control">
              <textarea name="message" class="form-control" minlength="50" maxlength="500" rows="3" required></textarea>
              <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group control-group">
            <label for="price" class="col-sm-4 control-label">Paybable Amount</label>
            <div class="col-sm-8 controls">
              <div class="input-group">
                <div class="input-group-addon">$</div>
                <input type="tel" name="price" class="form-control" id="price" placeholder="" required>
                <div class="input-group-addon">.00</div>
              </div>
              <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group control-group">
            <label for="audit_id" class="col-sm-4 control-label">Policy/App#</label>
            <div class="col-sm-8 controls">
              <input type="tel" pattern="\d+" maxLength="10" minLength="10" name="audit_id" class="form-control" id="audit_id" placeholder="" required>
              <p class="help-block"></p>
            </div>
          </div>

          <input type="hidden" value="{{$lead->id}}" name="id" />
          <input type="hidden" value="@if(isset($appointment->id)){{$appointment->id}} @endif" name="app_id" />


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Go Back</button>
          <button type="submit" class="btn btn-success" >Mark As Converted</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- Cancel Lead Modal End -->
