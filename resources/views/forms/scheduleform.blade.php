<!-- Schedule Appointment Modal Start -->
<div class="modal fade" id="scheduleForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Schedule Meeting With {{$lead->first_name}}</h4>
      </div>
<form class="form-horizontal" type="GET" action="{{route('schedule')}}" novalidate>
      <div class="modal-body">

          <h4>Location</h4>

          <div class="form-group control-group">
            <label for="location_name" class="col-sm-4 control-label">Name Of Place</label>
            <div class="col-sm-8 controls">
              <input type="text" name="location_name" class="form-control" id="location_name" placeholder="Example: Home or Starbucks" required>
              <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group control-group">
            <label for="city" class="col-sm-4 control-label">City</label>
            <div class="col-sm-8 controls">
              <input type="text" name="city" class="form-control" id="city" placeholder="Los Banos" required>
              <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group control-group">
            <label for="address_1" class="col-sm-4 control-label">Address 1</label>
            <div class="col-sm-8">
              <input type="text" name="address_1" class="form-control"  id="address_1" placeholder="123 Road Way" required>
              <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group control-group">
            <label for="address_2" class="col-sm-4 control-label">Address 2</label>
            <div class="col-sm-8 control">
              <input type="text" name="address_2" class="form-control" id="address_2" placeholder="Suite 2">

            </div>
          </div>


          <h4>Schedule</h4>

          <div class="form-group control-group">
            <label for="schedule_date"  class="col-sm-4 control-label">Date</label>
            <div class="col-sm-8 control">
              <input type="text" required name="schedule_date" class="form-control datepicker" id="schedule_date" />
              <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group control-group">
            <label for="schedule_time" class="col-sm-4 control-label">Time</label>
            <div class="col-sm-8 control">
              <input type="text" required name="schedule_time" class="form-control timePicker" id="schedule_time">
              <p class="help-block"></p>
            </div>
          </div>

          <h4>Tell us about this lead.</h4>
          <div class="control-group">
            <div class="control">
              <textarea name="schedule_note" class="form-control" minlength="50" maxlength="500" rows="3" required></textarea>
              <p class="help-block"></p>
            </div>
          </div>
          <input type="hidden" value="{{$lead->id}}" name="lead_id" />

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Schedule</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- Schedule Appointment Modal End -->
