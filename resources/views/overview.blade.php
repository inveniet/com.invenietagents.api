@extends('layouts.dashboard')

@section('title', 'Overview')

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3>Leads</h3>
      </div>
      @include('sidebars.leads')
      @if(!count($applicants))
      <br />
        @include('errors.noleads')
      @endif
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3>Appointments</h3>
      </div>
      @include('sidebars.appointments')
      @if(!count($schedules))
      <br />
        @include('errors.noscheduled')
      @endif
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3>Unpaid Leads</h3>
      </div>

      @if(!count($unpaid))
      <br />
        @include('errors.nounpaid')
      @else
      @include('components.unpaidleads')
      @endif
    </div>
  </div>
</div>

@endsection
