<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Validator;

class UsingKey
{

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // check if user is logged in
        if($request->user()){
          // if so, get his id
          $uid = $request->user()->id;

          // check if he has registerd a key:
          $activeKey = DB::select('select *  from invite_keys where userid = ?', [$uid]);

          if(count($activeKey)){
            // He has already registerd, lets let him through.
            return $next($request);
          }else{
            // He hasn't lets take him to the right place.
            return redirect()->route('registerKey');
          }

          // return $next($request);

        }
        return redirect()->route('home');

    }

}
