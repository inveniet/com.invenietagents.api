<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Validator;

class InviteKeyCheck
{

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

      $validator = Validator::make($request->all(), [
         'key' => 'required|exists:invite_keys,key,used,0'

     ]);

     if ($validator->fails()) {
         return redirect('/registerKey')
                     ->withErrors($validator)
                     ->withInput();
     }

        return $next($request);
    }

}
