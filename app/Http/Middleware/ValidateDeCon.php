<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Validator;

class ValidateDeCon
{

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

      $messages = [
        "unique" => "The App/Policy number you chose has already been used. Please double check it and try again."
      ];

      $validator = Validator::make($request->all(), [
         'audit_id' => 'required|unique:applicants,transaction_id'
     ], $messages);

     if ($validator->fails()) {
         return redirect()
                    ->route('scheduledAppointemtn', [$request->input("app_id")])
                     ->withErrors($validator)
                     ->withInput();
     }
        return $next($request);
    }

}
