<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth', 'usingKey']], function(){

  // ----------- Actions For Views ----------------- //

  // Home
  Route::get('/', ['as'=>'overview', 'uses'=>'LeadsController@showOverview']);

  // Leads
  Route::get('/dashboard', ['as'=>'home', 'uses'=>'LeadsController@showSplashDash']);
  Route::get('/dashboard/leads/{id}', ['as' => 'lead', 'uses' => 'LeadsController@showApplicant']);
  Route::get('/dashboard/leads/contact/{id}', [ 'as' => 'contactLead', 'uses' => 'LeadsController@contactApplicant']);

  // Pay leads
  Route::get('/payLead', ['as' => 'payLead', 'uses' => 'PayController@paymentPage']);
  Route::get('/thankyou', ['as' => 'thankyou', 'uses' => 'PayController@paymentRecieved']);

  // Schedule
  Route::get('/scheduled', ['as'=>'scheduled', 'uses'=>'LeadsController@showSplashScheduled']);
  Route::get('/scheduled/appointment/{id}', ['as'=>'scheduledAppointemtn', 'uses'=>'LeadsController@showAppointment']);

  // Converted
  Route::get('/converted', ['as'=>'converted', 'uses'=>'LeadsController@showConverted']);
  Route::get('/converted/info/{id}/{batch?}', ['as'=>'convertedInfo', 'uses'=>'LeadsController@showConvertedInfo']);

  // ----------- Actions For Forms ----------------- //

  Route::get('/prepareBatch', ['as' => 'prepareBatch', 'uses'=> 'PayController@logBatched']);
  Route::get('/cancelLead', ['as' => 'cancelLead', 'uses'=> 'LeadsController@cancelLead']);
  Route::get('/deferLead', ['as' => 'deferLead', 'uses'=> 'LeadsController@deferLead']);
  Route::get('/scheduleAppointment', ['as' => 'schedule', 'uses'=> 'LeadsController@scheduleAppointment']);
});



Route::group(['middleware' => ['auth', 'usingKey', 'validateDeCon']], function(){
  // ----------- Actions For Forms With DeCon middlewear ----------------- //
  Route::get('/declineLead', ['as' => 'declineLead', 'uses'=> 'LeadsController@declineLead']);
  Route::get('/convertLead', ['as' => 'convertLead', 'uses'=> 'LeadsController@convertLead']);

});





  // ----------- Actions For registerKey ----------------- //



Route::get('/registerKey', ['as' => 'registerKey', 'middleware' => 'auth', function(){
   return view('registerkey');
 }]);
Route::get('/registerKeyNow', ['as' => 'registerKeyNow','middleware' => ['auth', 'inviteKeyValidate'], 'uses'=> 'RegisterKeyController@registerNow']);

// ----------- Actions For API ----------------- //

Route::get('/new-applicant', 'ApiController@newApplicant');
Route::get('/request-token', 'ApiController@requestToken');

// ----------- Actions For Authenticate ----------------- //

Route::auth();

Route::get('/home', 'HomeController@index');
