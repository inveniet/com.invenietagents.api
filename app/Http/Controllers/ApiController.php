<?php

namespace App\Http\Controllers;

use Validator;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function newApplicant(Request $request)
    {
      $inputs = $request->all();
      // Validate incoming data
      $validator = Validator::make($inputs, [
        "addiction" => array( "required", "regex:/^(true|false)$/"),
        "aidsHiv" => array( "required", "regex:/^(true|false)$/"),
        "consent" => array( "required", "regex:/^(true|false)$/"),
        "contactDay" => array("required", "regex:/^\d{2}\/\d{2}\/\d{4}$/"),
        "contactTime" => array("required", "regex:/^([1-9]|[1][0-2])(pm|am|PM|AM)\-([1-9]|[1][0-2])(PM|AM|pm|am)$/"),
        "criminalCharges" => array( "required", "regex:/^(true|false)$/"),
        "diabetes" => array("required", "regex:/^(true|false)$/"),
        "disabilityBenefits" => array( "required", "regex:/^(true|false)$/"),
        "diseases" => array( "required", "regex:/^(true|false)$/"),
        "dob" =>  array("required", "numeric", "min:8", "regex:/^([0][0-9]|[1][0-2])([0][1-9]|[1-3][0-9])\d{4}$/"),
        "email" => "required|email",
        "first_name" => "required|alpha",
        "gender" => array( "required", "regex:/^(male|female)$/", "alpha"),
        "groupone" => array( "required", "regex:/^(true|false)$/"),
        "height" => array("required", "regex:/^[3-7][0-1][0-9]$/"),
        "hypertension" => array( "required", "regex:/^(true|false)$/"),
        "hypertensionMedication" => array("regex:/^(true|false)$/"),
        "last_name" => "required|alpha",
        "phone" => "required|numeric|min:10",
        "terms" => array( "required", "regex:/^(true|false)$/"),
        "tobacco" => array( "required", "regex:/^(true|false)$/"),
        "zip" => "required|numeric|min:5"
      ]);

      if($validator->fails()){
        return Response()->json([
          "response" => 100,
          "message" => "Validation Failed"
        ],
        200,
         array('Access-Control-Allow-Origin' => '*'));
      }
      // Process incoming data
      // Store incoming data
      DB::table('applicants')->insert($inputs);

      // Return response

      return Response()
      ->json([
        "response" => 200,
        "message" => $inputs
      ],
      200,
       array('Access-Control-Allow-Origin' => '*'));
    }

    public function requestToken(){
      return Response()
      ->json([
        "token" => csrf_token()
      ],
      200,
       array('Access-Control-Allow-Origin' => '*'));
    }
}
