<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LeadsController extends Controller
{

    // ------- Dashboard Actions ----------//

    /**
     * Show a list of all of the applicants.
     *
     * @return Response
     */
    public function showOverview()
    {
        $unpaidLeads = $this->getUnpaidConvertedLeads();
        $schedules = $this->getAppointemtnsList();
        $unpaidTotal = $this->getTotalUnpaidLeads();
        $applicants = DB::select('select first_name, last_name, id, viewed, called from applicants where converted = ? AND scheduled = ? AND cancelled = 0', [2, 0]);
        return view('overview', ['lead' => false, 'unpaidTotal' => $unpaidTotal, 'appointment' => false, 'applicants' => $applicants,'unpaid' => $unpaidLeads, 'schedules' => $schedules, ]);
    }
    /**
     * Show a list of all of the applicants.
     *
     * @return Response
     */
    public function showSplashDash()
    {
        $applicants = DB::select('select first_name, last_name, id, viewed, called from applicants where converted = ? AND scheduled = ? AND cancelled = 0', [2, 0]);
        return view('dashboard', ['applicants' => $applicants, 'lead' => false]);
    }
    /**
     * Show a list of all of the applicants.
     *
     * @return Response
     */
    public function showConverted()
    {
        $unpaidLeads = $this->getUnpaidConvertedLeads();
        $paidleads = $this->getPaidConvertedLeads();
        $paidBatches = $this->getPaidBatchedGroups();

        return view('converted', ['showing_batch'=>false,'paidLeads'=>false,'paidBatches' => $paidBatches, 'appointment'=> false, 'unpaid' => $unpaidLeads, 'paid' => $paidleads,'paidTotal'=> $this->getTotalUnpaidLeads(false), 'unpaidTotal' => $this->getTotalUnpaidLeads()]);
    }
    /**
     * Show a list of all of the applicants.
     *
     * @return Response
     */
    public function showConvertedInfo($id, $batch = false)
    {
        $unpaidLeads   = $this->getUnpaidConvertedLeads();
        $paidBatches   = $this->getPaidBatchedGroups();

        if($batch){
          $ids = DB::select('select lead_id from payment_batch where batch_id = ?', [$id]);

          $search_ids = [];
          foreach($ids as $key=>$di){
            $search_ids[$key] = $di->lead_id;
          }
          $convertedInfo = $this->getLeadsInfo($search_ids);
        }else{
          $convertedInfo = $this->getLeadsInfo([$id]);
        }

        return view('converted', ['showing_batch'=> $batch, 'paidLeads' => $convertedInfo,'paidBatches' => $paidBatches, 'unpaid' => $unpaidLeads, 'paidTotal'=> $this->getTotalUnpaidLeads(false), 'unpaidTotal' => $this->getTotalUnpaidLeads()]);
    }

    /**
     *
     */
    public function showApplicant($id)
    {
        // Set to viewed
        $affected = DB::update('update applicants set viewed = 1 where id = ?', [$id]);
        // Get list of applicants for sidebar
        $applicants = DB::select('select first_name, last_name, id, viewed, called from applicants where converted = ? AND scheduled = ? AND cancelled = 0', [2, 0]);
        // Get Main Lead
        $lead = $this->getApplicant($id);
        // Return lead
        return view('dashboard', ['applicants' => $applicants, 'lead' => $lead]);
    }

    public function contactApplicant($id){
        $affected = DB::update('update applicants set called = 1 where id = ?', [$id]);
        return redirect()->route('lead', [$id]);
    }




    // ------- Schedule Actions ----------//

    public function showSplashScheduled(){
        // Prepare info for sidebar:
        $schedules = $this->getAppointemtnsList();
        return view('scheduled', ['schedules' => $schedules, 'appointment' => false, 'lead' => false]);
    }

    /**
     * showAppointment
     *
     * Prepares the necessary information for a selected appointment view.
     */
    public function showAppointment($id){
      // Prepare info for sidebar:
      $schedules = $this->getAppointemtnsList();

      // Get info for single appointment
      $appointment = $this->getAppointment($id);
      $appointment_date = $appointment->schedule_date;
      $appointment_time = $appointment->schedule_time;
      $appointment->days_left = $this->timeLeft($appointment_date, $appointment_time);
      // Get applicant info for medical display:
      $applicantInfo = $this->getApplicant($appointment->lead_id);
      return view('scheduled', ['schedules' => $schedules, 'appointment' => $appointment, 'lead' =>$applicantInfo]);
    }

    // ------- Form Actions ----------//

    public function cancelLead(Request $request){
      $id = $request->input('id');
      $update = [
        'cancelled' => 1,
        'note' => $request->input('message')
      ];
      DB::table('applicants')->whereIn('id', [$id])->update($update);
      return redirect()->route('home');
    }
    public function deferLead(Request $request){
      $id = $request->input('id');
      $update = [
        'defered' => 1,
        'note' => $request->input('message')
      ];
      DB::table('applicants')->whereIn('id', [$id])->update($update);
      return redirect()->route('scheduled');
    }

    public function declineLead(Request $request){
      $id = $request->input('id');
      $update = [
        'converted' => 0,
        'price' => 0,
        'transaction_id' => $request->input('audit_id'),
        'note' => $request->input('message')
      ];
      DB::table('applicants')->whereIn('id', [$id])->update($update);
      return redirect()->route('scheduled');
    }

    public function convertLead(Request $request){

      $id = $request->input('id');
      $update = [
        'converted' => 1,
        'date_converted' => Carbon::now(),
        'price' => $this->leadPriceGenerator($request->input('price')),
        'transaction_id' => $request->input('audit_id'),
        'note' => $request->input('message')
      ];
      DB::table('applicants')->whereIn('id', [$id])->update($update);
      return redirect()->route('scheduled');
    }

    /**
     * scheduleAppointment
     *
     * Schedules an appointment based on the info inside teh get $request.
     */
    public function scheduleAppointment(Request $request){
      $inputs = $request->all();
      $id = $request->input('lead_id');

      DB::table('scheduled')->insert($inputs);

      // Set lead as scheduled
      $affected = DB::update('update applicants set scheduled = 1 where id = ?', [$id]);
      return redirect()->route('home');
    }

    // ------- Helper Methods ----------//

    /**
     *
     */
     private function leadPriceGenerator($price){
       $tiers   = [
          [.15, 0, 400],
          [.2, 401, 600],
          [.3, 601, 1000]
        ];
        $ammount = 0;
        foreach($tiers as $tier){
          if($tier[1] >= $price && $price <= $tier[2]){
            $ammount = $price*$tier[0];
            break;
          }
        }

        return money_format('%i', $ammount);

     }

    /**
     * timeLeft
     *
     * Calculates time left in days from $date (mm/dd/yyyy).
     */
    private function timeLeft($date, $time){
      $timeFirst = explode(":", $time);
      $timeSecond = substr($timeFirst[1], 0, 2);
      $ampm = substr($timeFirst[1], 2, 4);
      if($ampm == "pm"){
        $timeSecond = $timeSecond + 12;
      }
      $dateArray = explode( "/" ,  $date);
      $cdate = mktime($timeFirst[0], $timeSecond, 0, $dateArray[0], $dateArray[1], $dateArray[2]);
      $today = time();



      $initialDifference = $cdate - $today;
      if ($initialDifference < 0) {
        $difference = 0;
      }else{
        $difference = $initialDifference;
      }
      $hoursleft = floor($difference/60/24);
      if($hoursleft > 48){

          // A week a way
          if($hoursleft > 168){
            $daysleft = floor($difference/7/24/60/60);
            if($daysleft == 0){
            return "in one  week";
            }
            return "in ". $daysleft ." weeks";
          }else{
            $daysleft = floor($hoursleft/24);
            return "in ". $daysleft ." days";
          }

      }elseif($hoursleft <= 48 AND $hoursleft > 24){
        return "tomorrow";
      }else{

        return "in " . $hoursleft . " hours";
      }

    }

    /**
     * getApplicant
     *
     * Gets a single apaplicant aka: lead by its $id.
     */
    private function getApplicant($id){
      $lead = DB::select('select * from applicants where id = ?', [$id]);
      return $lead[0];
    }

    /**
     * getAppointment
     *
     * Gets a single appointment by its $id.
     */
    private function getAppointment($id){
      $appointment = DB::select('select * from scheduled where id = ?', [$id]);
      return $appointment[0];
    }

    /**
     * getAppointemtnsList
     *
     * Gets a list of appointments that have not been cancelled, shceduled or
     * converted for display in the leads sidebar.
     */
    private function getAppointemtnsList(){
      $schedules = DB::table('scheduled')
        ->join('applicants', 'scheduled.lead_id', '=', 'applicants.id')
        ->select('scheduled.id', 'applicants.defered', 'applicants.first_name', 'applicants.last_name', 'scheduled.schedule_time', 'scheduled.schedule_date')
        ->where('applicants.converted', '=', 2)
        ->where('applicants.cancelled', '=', 0)
        ->get();
              // Assign how many days left:
        foreach ($schedules as $key=>$value){
                $date = $value->schedule_date;
                $time = $value->schedule_time;
                $schedules[$key]->days_left =  $this->timeLeft($date, $time);
        }

        return $schedules;
    }

    /**
     *
     */
     private function getUnpaidConvertedLeads(){
       $unpayed = DB::select('select * from applicants where converted = ? AND payed = ?', [1, 0]);
       return $unpayed;
     }

     private function getTotalUnpaidLeads($unpaid = true){
       $total = 0;
       if($unpaid ){
          $unpayed = $this->getUnpaidConvertedLeads();
          foreach($unpayed as $key=>$value){
            $total = $total + $value->price;
          }
       }else{
         $unpayed = $this->getPaidBatchedGroups();
         foreach($unpayed as $key=>$value){
           $total = $total + $value->total;
         }
       }


       return money_format('%i', $total);
     }

    /**
     *
     */
     private function getPaidConvertedLeads(){
       $payed = DB::select('select * from applicants where converted = ? AND payed = ?', [1, 1]);
       return $payed;
     }

    /**
     *
     */
     private function getConvertedLeads(){
       $unpapaid = DB::select('select * from applicants where converted = ?', [1]);
       return $unpapaid;
     }

     private function getBatchedGroup($batch_id){
       $batch = DB::select('select * from payment_batch where batch_id = ?', [$batch_id]);
       return $batch;
     }

     private function getPaidBatchedGroups(){
       $payed_batch = DB::select('select distinct(batch_id), created, total from payment_batch where payed = ?', [1]);
       return $payed_batch;
     }

     private function getLeadsInfo($leads){
       $convertedInfo=[];
       foreach($leads as $key=>$id){
         $appointment = DB::select('select * from scheduled where lead_id = ?', [$id]);
         $lead = $this->getApplicant($id);
         $convertedInfo[$key] =
           ["lead"  => $lead,
               "appointment" => $appointment[0]

           ];
       }

       return $convertedInfo;

     }


}
