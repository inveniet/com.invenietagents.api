<?php

namespace App\Http\Controllers;

use Validator;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PayController extends Controller
{

  public function paymentPage(Request $request){
      $paying = $request->input('paying');
      $payable = DB::table('applicants')->wherein('id', $paying)->get();
      $total = 0;

      foreach($payable as $pay){
        $total = $pay->price + $total;
      }

      return view('checkout', ["payingList" => $payable, "total" => money_format('%i', $total)]);
  }

  public function logBatched(Request $request){
    // Create a unique ID
    $batch_transaction_id = uniqid();
    $payable_leads = $request->input('payablelead');
    $total = $request->input('total');
    $data_set = [];
    // Prepare data set
    foreach ($payable_leads as $p_l){
      $data_set[] = [
        'batch_id' => $batch_transaction_id,
        'lead_id' => $p_l,
        'total' =>$total
      ];
    }

    // Insert data
    DB::table('payment_batch')->insert($data_set);

    // Get environment variables if not, set to default ones
    $PAYPAL_LINK = env('PAYPAL_LINK', 'https://www.paypal.com/cgi-bin/webscr');
    $PAYPAL_HOSTED_BUTTON_ID = env('PAYPAL_HOSTED_BUTTON_ID', 'EAZWDNCV3NFRL');
    $PAYPAL_CMD= env('PAYPAL_CMD', '_s-xclick');
    $PAYPAL_RETURN= env('PAYPAL_RETURN', 'http://sandbox.api.invenietagents.com');
    $PAYPAL_DIGITAL_TOKEN= env('PAYPAL_DIGITAL_TOKEN', 'http://sandbox.api.invenietagents.com');

    return view('predispatchPaypal', ["paypal_token"=> $PAYPAL_DIGITAL_TOKEN, "paypal_return" => $PAYPAL_RETURN, "paypal_cmd" => $PAYPAL_CMD, "paypal_hosted_button_id" => $PAYPAL_HOSTED_BUTTON_ID, "paypal_link" => $PAYPAL_LINK, "batch_id" => $batch_transaction_id, "total" => $total]);
  }

  public function paymentRecieved(Request $request){
    $batch_id = $request->input('item_number');
    $completed = $request->input('st');
    $amount = $request->input('amt');

    if($completed  == "Completed"){
      // Update batch as payed
      $affected = DB::update('update payment_batch set payed = 1 where batch_id = ?', [$batch_id]);
      $ids = DB::select('select lead_id from payment_batch where batch_id = ?', [$batch_id]);

      $new_id = [];
      foreach($ids as $key=>$value){
        $new_id[$key] = $value->lead_id;
      }

      DB::table('applicants')->whereIn('id', $new_id)->update(array('payed' => 1));

      // Update individual as payed
    }

    return view('thankyou', ["batch_id" => $batch_id, "amount" => $amount ]);

  }

}
