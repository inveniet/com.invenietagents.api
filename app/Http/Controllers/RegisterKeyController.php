<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterKeyController extends Controller
{

  public function registerNow(Request $request){
    // test
    // Get user id
    $uid = $request->user()->id;
    $key = $request->input('key');

    // Update database where key is equal to the one in the request and add UID to it.
    $update = [
      'used' => 1,
      'userid' => $uid
    ];

    DB::table('invite_keys')->whereIn('key', [$key])->update($update);
    // Send 'em home.
    return redirect()->route('home');

  }

}
